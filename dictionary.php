<?php

$dictionary = json_decode(file_get_contents('src/Json/json.json'));


$search = trim($_GET['search']);
$result = [];

if(!empty($search)) {
foreach($dictionary as $country => $cities) {
    foreach($cities as $city) {
    if (strpos($city, $search) !== false) {
        //$result[$key] = $value;
        array_push($result, $city);
    }
}
}
}
$result = array_unique($result);

echo json_encode($result);
